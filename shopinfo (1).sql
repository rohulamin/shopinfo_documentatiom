-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 18, 2018 at 02:48 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopinfo`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'SamSung', '2017-09-18 23:11:52', '2017-09-18 23:11:52'),
(3, 'Sony', '2017-09-29 22:05:07', '2017-09-29 22:05:07'),
(4, 'Asus', '2017-10-04 09:10:33', '2017-10-04 09:10:33'),
(5, 'Dell', '2017-10-04 09:10:39', '2017-10-04 09:10:39'),
(6, 'Lenovo', '2017-10-04 09:11:06', '2017-10-04 09:11:06'),
(8, 'A Data', '2017-10-07 11:43:44', '2017-11-08 15:30:12');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Clothes', '2017-10-12 03:23:41', '2017-10-12 03:23:41'),
(3, ' Jewellery', '2017-10-19 14:53:26', '2017-10-19 14:53:26'),
(4, 'Electronics', '2017-07-11 20:20:12', '0000-00-00 00:00:00'),
(6, 'Mobile', '2017-07-11 20:20:46', '0000-00-00 00:00:00'),
(8, 'Computer', '2017-09-22 06:52:02', '2017-09-22 06:52:02'),
(9, 'Food Corner', '2017-10-19 08:53:05', '2017-10-19 08:53:05');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Dhaka', '2017-07-11 20:21:58', '0000-00-00 00:00:00'),
(2, 'Chittagong', '2017-07-11 20:21:58', '0000-00-00 00:00:00'),
(4, 'Coxes Bazar', '2017-09-05 22:45:13', '2017-09-05 22:45:13'),
(5, 'Khulna', '2017-09-10 12:54:23', '0000-00-00 00:00:00'),
(6, 'Rajshahi', '2017-09-10 12:55:13', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'not null',
  `cities_id` tinyint(3) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `cities_id`, `created_at`, `updated_at`) VALUES
(3, 'Kuril', 1, '2017-07-12 16:30:59', '0000-00-00 00:00:00'),
(4, 'lohagara', 2, '2017-09-05 19:22:20', '2017-09-05 19:22:20'),
(8, 'Agrabad', 2, '2017-09-08 04:55:52', '2017-09-08 04:55:52'),
(10, 'New Market', 2, '2017-09-08 04:57:00', '2017-09-08 04:57:00'),
(11, 'Kawran Bazar', 1, '2017-09-09 11:28:52', '2017-09-09 11:28:52'),
(12, 'Elephant Road', 1, '2017-09-10 07:00:42', '2017-09-10 07:00:42'),
(14, 'Motijheel', 1, '2017-10-05 07:57:39', '2017-10-05 01:57:18'),
(22, 'kolatoli', 4, '2017-10-09 21:12:02', '2017-10-09 21:12:02'),
(23, 'Chokoria', 4, '2017-10-09 21:12:27', '2017-10-09 21:12:27'),
(24, 'Ramna', 1, '2017-10-17 23:52:55', '2017-10-17 23:52:55'),
(25, 'Azimpur', 1, '2017-11-14 05:22:40', '2017-11-14 05:22:40');

-- --------------------------------------------------------

--
-- Table structure for table `markets`
--

CREATE TABLE `markets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `locations_id` smallint(5) UNSIGNED NOT NULL,
  `latlang` longtext COLLATE utf8_unicode_ci,
  `road_no` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `house_no` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_code` smallint(4) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `markets`
--

INSERT INTO `markets` (`id`, `name`, `picture`, `locations_id`, `latlang`, `road_no`, `house_no`, `post_code`, `created_at`, `updated_at`) VALUES
(1, 'Bashundhara City', 'jpg', 11, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3651.894847422042!2d90.38866281498136!3d23.751128884589033!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8bcd681372b%3A0x5c2b8755e4327004!2sBashundhara+City+Shopping+Complex!5e0!3m2!1sen!2sbd!4v151120052537', 'Pantha Path', '46/a4', 1208, '2017-11-20 18:07:22', '2017-10-17 23:49:36'),
(2, 'Eastern Plaza', 'jpg', 24, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1826.0493480682187!2d90.39068289106797!3d23.743859650117212!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8be61f9fe79%3A0x9f2288c238c55fda!2z4KaH4Ka44KeN4Kaf4Ka-4Kaw4KeN4KaoIOCmquCnjeCmsuCmvuCmnOCmviDgprbgpqrgpr_gpoIg4KaV4Kau4Kaq4KeN4Kay4KeH4KaV4KeN4Ka4!5e0!3m2!1sbn!2sbd!4v1509639397456', 'Hatirpul', '345', 1212, '2017-11-13 23:21:53', '2017-10-17 23:57:58'),
(3, 'Jamuna Future Park', 'jpg', 3, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1825.0709508178202!2d90.42324585495082!3d23.813552561417616!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c64d7ecaeb5f%3A0xd912585bbda6d9c2!2sJamuna+Future+Park!5e0!3m2!1sen!2sbd!4v1511111593521', 'Bashundhara ', 'Progati Sarani', 1209, '2017-11-19 17:17:09', '2017-10-18 00:02:33'),
(4, 'New Market', '', 25, '', 'Mirpur Road', '456', 2455, '2017-11-19 20:36:05', '2017-11-19 20:36:05'),
(7, 'New Market', '', 8, '', 'access Road', '456', 4100, '2017-11-19 20:40:11', '2017-11-19 20:40:11');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `stall_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float(8,2) DEFAULT NULL,
  `vat` float(4,2) DEFAULT NULL,
  `discount` float(4,2) DEFAULT NULL,
  `units_id` tinyint(3) UNSIGNED NOT NULL,
  `pic1` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `stall_id`, `quantity`, `price`, `vat`, `discount`, `units_id`, `pic1`, `status`, `created_at`, `updated_at`) VALUES
(2, 'bargher', '', 14, 5, 87.00, 2.00, 10.00, 13, 'jpg', NULL, '2017-11-08 07:35:14', '0000-00-00 00:00:00'),
(4, 'lichi', NULL, 14, 6, 67.00, 2.00, 8.00, 13, 'jpg', NULL, '2017-10-21 04:18:37', '2017-10-21 04:18:37'),
(6, 'samusa', NULL, 14, 6, 8.00, 0.00, 0.00, 13, 'jpg', NULL, '2017-10-21 05:06:54', '2017-10-21 05:06:54'),
(7, 'Synphony w65', 'Symphony B12\r\n\r\nMP3, MP4, FM\r\nBluetooth\r\n800 mAh Li-ion Battery\r\n1.77\" TFT QQVGAAndroid Kitkat 4.4\r\n3.5\" HVGA TN Display\r\n1.3 MP + 0.3MP Camera\r\n1.3 GHz Dual Core\r\n256 MB RAM + 512MB ROM\r\n1300 mAh Li-ion BatteryAndroid Kitkat 4.4\r\n3.5\" HVGA TN Display\r\n1.3 MP + 0.3MP Camera\r\n1.3 GHz Dual Core\r\n256 MB RAM + 512MB ROM\r\n1300 mAh Li-ion Batteryjh', 9, 3, 5500.00, 2.00, 3.00, 12, 'jpg', NULL, '2017-10-25 16:03:06', '2017-10-21 08:04:22'),
(8, ' Electronics House Digital Voice Recorder With Mp3 Player 16GB – Black', NULL, 5, 4, 3950.00, 0.00, 0.00, 13, 'jpg', NULL, '2017-10-22 12:51:57', '2017-10-22 12:51:57'),
(9, 'Zz Fusion Printed White Cotton Short Sleeves T-Shirt For Women', 'Product Type: T-Shirt\r\nColor: Printed White\r\nMain Material: Cotton\r\nGender: Women\r\nStyle: Short Sleeves\r\nBrand new and exclusive design', 3, 4, 760.00, 0.00, 5.00, 13, 'jpg', NULL, '2017-10-26 02:06:47', '2017-10-25 19:49:46'),
(10, 'T-Shirt  Black Cotton Ideas Are Bullet Proof T-shirt For Men', 'Bullet Proof T-shirt For Men', 3, 5, 250.00, 2.00, 5.00, 13, 'jpg', NULL, '2017-10-25 20:11:27', '2017-10-25 20:11:27'),
(11, 'Crazefreak Deep Green Cotton Goal T-shirt For Men', 'Product Type: T-shirt\r\nColor: Deep Green\r\nMain Material: Cotton\r\nGender: Men', 3, 2, 250.00, 0.00, 3.00, 13, 'jpg', NULL, '2017-10-25 20:14:44', '2017-10-25 20:14:44'),
(12, 'KEY FEATURES Product Type: Jeans Color: Blue Main Material: Denim Gender: Men', 'SPECIFICATIONS OF DENIM CASUAL REGULAR FIT JEANS - BLUE\r\nSKU	CO741FA12XVLCNAFAMZ\r\nWeight (kg)	0.2\r\nColour	Blue\r\nMain material	Denim\r\nType	Jeans\r\nPattern	Regular Fit\r\nCare label	Mid Wash\r\nStyle	Casual', 3, 2, 750.00, 0.00, 5.00, 13, 'jpg', NULL, '2017-10-25 20:23:31', '2017-10-25 20:23:31'),
(14, 'B.CO White Cotton Panjabi For Men', 'B.CO is a famous local brand who sells various items at a lower cost. This helps customers to get all of their expected products at a reasonable price. They have been running their business very successfully. Their products assure quality as their top priority is customer satisfaction through their products and service. Many products can be acquired from this brand.\r\nKEY FEATURES\r\nProduct Type: Panjabi\r\nMain Material: Cotton\r\nColor: White\r\nGender: Men\r\nUnique Design\r\nUnique Collection', 1, 4, 1400.00, 0.00, 3.00, 13, 'jpg', NULL, '2017-11-01 14:01:00', '2017-11-08 15:28:43'),
(15, 'CURRENT BD Red Mix Cotton Traditional Panjabi For Menii', 'About CURRENT BD\r\nCURRENT BD is a trusted and reliable source for all your garment related needs from Bangladesh. CURRENT BD manufactures and supplies quality products in all categories at a competitive price range from their own.\r\nKEY FEATURES\r\nProduct Type: Panjabi\r\nColor: Red\r\nMain Material: Mix Cotton\r\nGender: Men\r\n', 1, 8, 1450.00, 0.00, 2.00, 13, 'jpg', NULL, '2017-11-08 15:24:35', '2017-11-08 15:28:20'),
(16, ' Apple iMAC 21.5\" (MMQA2ZP/A) Core i5 8GB RAM 1TB Storage (2017)', 'Product Code:	IMAC 21.5\" MMQA2ZP/A\r\nBrand:	Apple\r\nDesktop PC\r\nProcessor Type	Intel 7th Gen 2.3GHz dual‑core i5 (Turbo Boost up to 3.6GHz)\r\nProcessor Speed	2.3GHz dual‑core i5 (Turbo Boost up to 3.6GHz)\r\nRAM	8GB of 2133MHz DDR4 onboard memory\r\nHard Disk	1TB (5400-rpm)\r\nMonitor	21.5-inch (diagonal) LED‑backlit 1920x1080 resolution with support for millions of colours display\r\nGraphics Card	Intel Iris Plus Graphics 640\r\nLan	Wi-Fi-802.11ac Wi-Fi wireless networking, IEEE 802.11a/b/g/n compatible II Bluetooth- 4.2 wireless technology II\r\nAudio	Stereo speakers II Microphone II 3.5mm headphone jack II Support for Apple iPhone headset with microphone II\r\nOperating system	macOS Sierra\r\nMouse	Apple Magic Mouse 2\r\nKeyboard	Apple Magic Keyboard\r\nInterface	3.5 mm headphone jack, SDXC card slot, Four USB 3 ports (compatible with USB 2), Two Thunderbolt 3 (USB-C) ports with support for: DisplayPort, Thunderbolt (up to 40 Gbps), USB 3.1 Gen 2 (up to 10 Gbps), Thunderbolt 2, HDMI, DVI, and VGA supported using adapters (sold separately), 10/100/1000BASE-T Gigabit Ethernet (RJ-45 connector), Kensington lock slot\r\nDescription	\r\nApple iMAC 21.5\" (MMQA2ZP/A) Core i5 8GB RAM 1TB Storage (2017)\r\nProcessor: Intel 2.3GHz dual‑core i5 (Turbo Boost up to 3.6GHz)\r\nMemory: 8GB of 2133MHz DDR4 onboard memory\r\nStorage: 1TB (5400-rpm) hard drive\r\nGraphics: Intel Iris Plus Graphics 640\r\nDisplay: 21.5-inch (diagonal) LED‑backlit 1920x1080 resolution with support for millions of colours display\r\nAudio: Stereo speakers, Microphone, 3.5 mm headphone jack\r\nWireless:802.11ac Wi-Fi wireless networking, IEEE 802.11a/b/g/n compatible, Bluetooth 4.2 wireless technology\r\nSize and Weight: Height: 45.0 cm (17.7 inches), Width: 52.8 cm (20.8 inches), Stand depth: 17.5 cm (6.9 inches), Weight: 5.66 kg (12.5 pounds)\r\nOperating System: macOS Sierra\r\nOne year Limited Warranty (parts & labor)', 22, 6, 54000.00, 5.00, 10.00, 13, 'jpg', NULL, '2017-11-18 01:18:15', '2017-11-18 01:18:15');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Shirt', '2017-09-27 13:50:59', '0000-00-00 00:00:00'),
(2, 'Shirt', '2017-09-27 13:51:11', '0000-00-00 00:00:00'),
(3, 'pant', '2017-09-27 13:51:29', '0000-00-00 00:00:00'),
(4, 'Laptop', '2017-09-27 13:51:29', '0000-00-00 00:00:00'),
(5, 'pant', '2017-09-27 13:51:36', '0000-00-00 00:00:00'),
(6, 'Laptop', '2017-09-27 13:51:36', '0000-00-00 00:00:00'),
(7, 'Television', '2017-09-29 22:05:56', '2017-09-29 22:05:56'),
(8, 'Mobile', '2017-10-03 19:38:29', '2017-10-03 19:38:29');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` text COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serial` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stalls`
--

CREATE TABLE `stalls` (
  `id` int(10) UNSIGNED NOT NULL,
  `users_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `markets_id` int(10) UNSIGNED NOT NULL,
  `categories_id` tinyint(3) UNSIGNED NOT NULL,
  `stall_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `working_time` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `clossing_day` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bussines_type` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stalls`
--

INSERT INTO `stalls` (`id`, `users_id`, `name`, `markets_id`, `categories_id`, `stall_no`, `phone`, `mobile`, `email`, `working_time`, `clossing_day`, `website`, `bussines_type`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'AABARTAN', 3, 1, 'Shop # GD-005', '028363551', '01715665758', 'aabartan@yahoo.com', '90:00-20:00', 'SUN Day', 'www.aabartan.com', 'Gents: Shirt,T-Shirt, Pant\'s, Panjabi, Fotuya, etc. Ladies:Salawar Kameez, Palazzo,Tops,Sharee etc.(All Kinds of Fashionable Item).', 'jpg', NULL, '2017-11-08 07:32:06', '2017-10-18 00:14:07'),
(2, 6, 'Amreen', 3, 1, 'Shop # -013, Zone # C(Ground Floor)', '01926607779', '01712646984', 'NILL', '11:00-21:00', 'Sunday', 'NILL', 'Bag, shoes & Jewelry,(All Kinds of Fashionable Item).', 'jpg', NULL, '2017-10-19 07:01:15', '2017-10-19 07:01:15'),
(3, 1, 'ARTISAN', 3, 1, 'Shop # -01/A06-07 Ka-244,(1st Floor)', '029823238', '01816911535', 'artisanoutfittersltd@gmail.com', '11:00-21:00', 'Sunday', 'NILL', 'Gents: Shirt\'s, Pant\'s, Shoes, etc. Ladies: Salwar Kameez, Frog,Tops, Shoes, etc. Kids: Shirt\'s, Pant\'s, Shoes, etc. (All Kinds of Fashionable Item).', 'jpg', NULL, '2017-10-19 07:13:31', '2017-10-19 07:13:31'),
(4, 3, 'Banglar Mela', 3, 1, ' Shop # GC-043,', '01678086533', '01828960200', 'mabanp@gmail.com', '11:00-21:00', 'Sunday', '', 'Gents: Shirt,T-Shirt, Pant\'s, Panjabi, Fotuya, etc. Ladies:Salawar Kameez, Palazzo,Tops,Sharees etc.', 'jpg', NULL, '2017-10-19 07:17:30', '2017-10-19 07:17:30'),
(5, 4, 'A.R. ELECTRONICS', 3, 4, 'Ka-244, Shop No- UA-023', '029823220', '029823331', '', '11:00-21:00', 'Wednesday', '', 'All Kinds of Colour LCD, LED TV, Air Conditioner, Telephone Set, Refrigerator, Washing Machine, Sound Box, and Electrical Equipment Available Here.', 'jpg', NULL, '2017-10-19 07:24:29', '2017-10-19 07:24:29'),
(6, 3, 'M.K. Electronics', 3, 4, ' Shop No- UA 006', '01823055876', '029823036,029823037', 'jfp@mkelectronics.org', '11:00-21:00', 'Wednesday', '', 'All Kinds of Colour LCD, LED TV, Air Conditioner, Telephone Set, Refrigerator, Washing Machine, Microwave Oven, Sound Box, Fan and Electrical Equipment Available Here.', 'jpg', NULL, '2017-10-19 07:33:01', '2017-10-19 07:33:01'),
(7, 4, 'VISION', 3, 4, '(Ground Floor)', '9613737777', '', '', '11:00-21:00', 'Tuesday', '', 'All Kinds of 3D TV, LED TV,Home Theater, DVD Player & Home Appliance Available Here.', 'jpg', NULL, '2017-10-19 07:38:01', '2017-10-19 07:38:01'),
(8, 1, 'Fisher & Paykel', 3, 4, ' Shop No- UA/13, Basement-1, ', '029823020', '01728646758', '', '11:00-21:00', 'Wednesday', '', 'All Kinds of Colour LCD, LED TV, Air Conditioner, Telephone Set, Refrigerator, Washing Machine, Microwave Oven, Sound Box, and Electrical Equipment Available Here.', 'jpg', NULL, '2017-10-19 07:40:23', '2017-10-19 07:40:23'),
(9, 6, 'active device', 3, 6, 'Shop # 36, Level-04, Block-C, ', '029823234', '01799000888', '', '11:00-21:00', 'Sunday', '', 'Samsung, htc, Nokia, Asus, Apple, Sony, Black Berry Mobile (All Kids of Mobile)', 'jpg', NULL, '2017-10-19 14:42:42', '2017-10-19 08:10:56'),
(10, 4, 'SAMSUNG smart phone cafe', 3, 6, 'Shop # 018, Level-04, Block-C, ', '', '01777754224', 'samsung.missedcall@gmail.com', '11:00-21:00', 'Sunday', '', 'Samsung Mobile Brand Outlate.', 'jpg', NULL, '2017-10-19 08:46:47', '2017-10-19 08:46:47'),
(11, 5, 'SYMPHONY SMART', 3, 6, 'hop # 018, Level-04, Block-C, ', '', '01799004444', 'symphony.missedcall@gmail.com', '11:00-21:00', 'Wednesday', '', 'SYMPHONY BRAND OUTLET.', 'jpg', NULL, '2017-10-19 08:48:30', '2017-10-19 08:48:30'),
(12, 6, 'm2m Communications', 3, 6, ' Shop # 020A, Level-04, Block-C', '01982766233', '01912650363', 'nirob6936@yahoo.com', '11:00-21:00', 'Sunday', 'www.m2m.com', 'Huawei Mobile(Huawei Brand Outlate ).', 'jpg', NULL, '2017-11-18 06:43:09', '2017-10-19 08:50:28'),
(14, 5, 'Pizza Hut', 3, 9, ' Shop # 5C-013(5th Floor), Ka-244Kuril,', '', '01777768098', 'phjamunafuturepark@tfl.transcombd.com', '11:00-21:00', 'Sunday', '', 'Bangla, Indian, Chinese & Fast Food.', 'jpg', NULL, '2017-10-19 08:57:29', '2017-10-19 08:57:29'),
(15, 4, 'Spicy Chicken & Masala', 3, 9, 'Shop # 19,20,21 & 22, Lavel # 5, Zone # C, Kuril', '01726329102', '01726344108', '', '11:00-21:00', '	Wednesday', '', 'A Indian, Thai & Chinese Restaurant.', 'jpg', NULL, '2017-10-19 08:59:05', '2017-10-19 08:59:05'),
(16, 6, 'AL-ARAFA BORKA BAZAR', 1, 1, ' Lavel # 4, Block # D,Shop # 22, ', '', '1819466304', '', '10:00-20:00', '	Tuesday', '', 'All Kinds of Design\'s Borka, Orna, Hezab, Seup Nekup,etc.', 'jpg', NULL, '2017-10-19 09:02:46', '2017-10-19 09:02:46'),
(17, 5, 'Anjan Sharees', 1, 1, 'Lavel # 4,Block # C, Shop # 86-87 Bashundhara City ', '0291114440 ', '01833519182', '', '10:00-20:00', 'Tuesday', '', 'Sharees.', 'jpg', NULL, '2017-10-19 09:04:40', '2017-10-19 09:04:40'),
(18, 5, 'Babie’s Gallerie', 1, 1, 'Bashundhara City, Lavel # 1, Block # D, Shop # 26, ', '029111440', '028158025', '', '10:00-20:00', 'Tuesday', '', 'Products	A Complete Fashion House for Babies (Tops, Salwar, Kamiz etc.)', 'jpg', NULL, '2017-10-22 03:32:50', '2017-10-22 03:32:50'),
(22, 16, 'Rafi Comouter House', 3, 8, '123/4, Four Floor, Block B', '', '01911615431', 'rasel.cpi08@gamil.com', '10:00-20:00', 'Tuesday', 'www.raficiomputer.com', 'Computer items', 'jpg', NULL, '2017-11-18 16:34:08', '2017-11-18 01:11:00'),
(23, 10, 'BARNALI', 2, 1, 'Shop # 3/22, Eastarn Plaza, Sonargaon Road, Hatirpool, Dhaka-1205', '017245678', '028614637', '', '10:00-20:00', 'Tuesday', 'www.banali.com', 'Gents: Pant\'s, Shirt\'s, T- Shirt\'s, etc Ladies: Tops, Ties, Frog etc.', 'jpg', NULL, '2017-11-18 11:57:59', '2017-11-18 11:57:59'),
(24, 17, 'Rafi Greel House', 3, 9, '524', '988765', '0172524675', 'greel@gmail.com', '09:00-20:00', 'no', 'nill', 'Food', '', NULL, '2017-11-20 02:13:07', '2017-11-19 19:58:33'),
(25, 17, 'Rafi Greel House', 1, 9, '', '', '', '', '', '', '', '', 'jpg', NULL, '2017-11-20 02:13:15', '2017-11-19 19:59:52'),
(26, 17, 'rafi Mobile House', 1, 4, '', '', '', '', '', '', '', '', 'jpg', NULL, '2017-11-19 20:14:56', '2017-11-19 20:14:56'),
(27, 17, 'rafi Mobile House', 3, 4, '', '', '', '', '', '', '', '', '', NULL, '2017-11-19 20:15:30', '2017-11-19 20:15:30'),
(31, 17, 'Banban Jewellery', 1, 3, '5', '', '', '', '', '', '', '', '', NULL, '2017-11-19 20:23:00', '2017-11-19 20:23:00'),
(39, 17, 'baba', 1, 1, '5', '', '', '', '', '', '', '', 'jpg', NULL, '2018-01-11 18:11:49', '0000-00-00 00:00:00'),
(42, 17, 'ma jewelars', 1, 3, '5676', '', '', '', '', '', '', '', 'jpg', NULL, '2018-01-11 17:30:19', '2017-11-19 20:32:36'),
(43, 1, 'Amin jewellary', 7, 3, '45', '', '', '', '', '', '', '', 'jpg', NULL, '2018-01-11 16:15:12', '2017-11-19 20:41:54');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `created_at`, `updated_at`) VALUES
(12, 'Pice', '2017-11-08 15:22:30', '0000-00-00 00:00:00'),
(13, 'pices', '2017-11-08 15:21:03', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `contact`, `gender`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Rohul Amin', 'amin@gmail.com', '202cb962ac59075b964b07152d234b70', '01869751400', 'M', 'A', '1', '2017-11-15 17:37:23', '0000-00-00 00:00:00'),
(2, 'Abdul Bari', 'aabartan@yahoo.com', '202cb962ac59075b964b07152d234b70', '01720053742', 'M', 'C', '1', '2017-11-21 12:59:10', '2017-10-18 00:11:28'),
(3, 'Asif Murad', 'asif@gmail.com', '202cb962ac59075b964b07152d234b70', '016543267', 'M', 'C', ' 1', '2017-11-08 07:44:42', '2017-10-19 03:35:33'),
(4, 'Md. Rasel', 'rasel@gmail.com', '202cb962ac59075b964b07152d234b70', '0167654356', 'M', 'C', '1', '2017-11-21 13:00:35', '2017-10-19 03:37:45'),
(5, 'Amena Khatun', 'amena@gmail.com', '202cb962ac59075b964b07152d234b70', '01987654', 'M', 'C', '1', '2017-11-15 17:14:21', '2017-10-19 03:39:12'),
(6, 'Jahid Hasan', 'jahid@gmail.com', '202cb962ac59075b964b07152d234b70', '0178976543', 'M', 'C', ' ', '2017-11-08 07:44:28', '2017-10-19 03:41:10'),
(7, 'Shibli', 'shibli@gmail.com', '202cb962ac59075b964b07152d234b70', '0187265266', 'M', 'C', '1', '2017-11-08 07:44:46', '2017-10-22 03:39:35'),
(8, 'Habib Ullah', 'habib@gmail.com', '202cb962ac59075b964b07152d234b70', '0197678987', 'M', 'C', 'C', '2017-10-22 03:40:21', '2017-10-22 03:40:21'),
(9, 'Samsul Nahar', 'nahar@gmail.com', '202cb962ac59075b964b07152d234b70', '01869751432', 'M', 'C', '1', '2017-11-21 12:59:21', '2017-11-13 12:29:07'),
(10, 'fatema akter', 'fatema@gmail.com', '202cb962ac59075b964b07152d234b70', '0156345667', 'M', 'C', '1', '2017-11-21 12:59:32', '2017-11-13 12:35:54'),
(11, 'abdul karim', 'karim@gmail.com', '202cb962ac59075b964b07152d234b70', '0192837366', 'M', 'C', '1', '2017-11-21 12:59:43', '2017-11-13 13:54:10'),
(12, 'Somon Ali', 'somon@gmail.com', '202cb962ac59075b964b07152d234b70', '019876548', 'M', 'C', '1', '2017-11-13 20:19:59', '2017-11-13 14:17:14'),
(14, 'rakib Hasan', 'ami@gmail.com', '202cb962ac59075b964b07152d234b70', '016543567', 'M', 'V', '1', '2017-11-14 09:41:34', '2017-11-14 03:02:12'),
(15, 'Sabbir Hossain', 'sabbir@gmail.com', '202cb962ac59075b964b07152d234b70', '01987654', 'M', 'V', '1', '2017-11-16 15:29:48', '2017-11-16 09:24:11'),
(16, 'Md Rasel', 'rasel.cpi08@gamil.com', '202cb962ac59075b964b07152d234b70', '01911615431', 'M', 'C', '1', '2017-11-21 13:00:58', '2017-11-17 21:59:47'),
(17, 'Rafi', 'rafi@gmail.com', '202cb962ac59075b964b07152d234b70', '019876544', 'M', 'V', ' ', '2017-11-19 19:36:09', '2017-11-19 19:36:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`,`cities_id`),
  ADD KEY `cities_id` (`cities_id`);

--
-- Indexes for table `markets`
--
ALTER TABLE `markets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`,`locations_id`),
  ADD KEY `locations_id` (`locations_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `units_id` (`units_id`),
  ADD KEY `stall_id` (`stall_id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stalls`
--
ALTER TABLE `stalls`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`,`markets_id`),
  ADD UNIQUE KEY `name_2` (`name`,`markets_id`,`stall_no`),
  ADD UNIQUE KEY `name_3` (`name`,`markets_id`,`stall_no`),
  ADD UNIQUE KEY `name_4` (`name`,`markets_id`,`stall_no`),
  ADD KEY `users_id` (`users_id`),
  ADD KEY `markets_id` (`markets_id`),
  ADD KEY `categories_id` (`categories_id`),
  ADD KEY `name_5` (`name`,`markets_id`,`stall_no`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `markets`
--
ALTER TABLE `markets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stalls`
--
ALTER TABLE `stalls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `locations`
--
ALTER TABLE `locations`
  ADD CONSTRAINT `locations_ibfk_1` FOREIGN KEY (`cities_id`) REFERENCES `cities` (`id`);

--
-- Constraints for table `markets`
--
ALTER TABLE `markets`
  ADD CONSTRAINT `markets_ibfk_1` FOREIGN KEY (`locations_id`) REFERENCES `locations` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`units_id`) REFERENCES `units` (`id`),
  ADD CONSTRAINT `products_ibfk_4` FOREIGN KEY (`stall_id`) REFERENCES `stalls` (`id`);

--
-- Constraints for table `stalls`
--
ALTER TABLE `stalls`
  ADD CONSTRAINT `stalls_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `stalls_ibfk_2` FOREIGN KEY (`markets_id`) REFERENCES `markets` (`id`),
  ADD CONSTRAINT `stalls_ibfk_3` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
